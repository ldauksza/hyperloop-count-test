class Customer < ApplicationRecord
  regulate_scope all: :always_allow
  regulate_scope unscoped: :always_allow
end
